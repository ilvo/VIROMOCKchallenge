## Dataset 9: Viral concentration of different genomic segments (Emaraviruses on Pistacio)

### Introduction to the dataset

The real dataset is composed of *Pistacia emaravirus B* (PiVB), a newly discovered Emaravirus from pistachio tree [(Buzkan et al., 2019)](https://www.sciencedirect.com/science/article/abs/pii/S0168170218306373). **The limit tested is the ability to detect all the genomic segments with different concentrations**. The viral genome is composed of seven distinct negative-sense, single-stranded RNAs, showing different frequencies in the dataset.

The different labs that analyzed the dataset are listed in Table 1.

*Table 1: Participants to the VIROMOCK challenge of Dataset 9.*

| Participant                      	| Institute 	| Country 	| email                                  	|
|----------------------------------	|-----------	|---------	|----------------------------------------	|
| Lucie Tamisier                   	| ULg       	| Belgium 	| <lucie.tamisier@uliege.be>             	|
| Annelies Haegeman, Yoika Foucart 	| ILVO      	| Belgium 	| <annelies.haegeman@ilvo.vlaanderen.be> 	|
| Alex Hu 	| USDA-APHIS      	| USA 	| <xiaojun.hu@usda.gov> 	|
| Miguel Morard 	| ValGenetics SL     	| Spain 	| <mmorard@valgenetics.com> 	|


The observed values of the different participants are listed in Table 2.

*Table 2: Observed composition of Dataset 9 after analysis by different labs.*

| Institute/Lab | Virus/viroid | Observed   closest NCBI accession | Observed   proportion of PiVB fragment reads (%)<sup>1</sup>  | Were   you able to detect all PiVB fragments? |
|:-------------:|--------------|-----------------------------------|--------------------------------------------------|-----------------------------------------------|
|      ILVO     | RNA1         | MH727572                          | 27                                               | yes                                           |
|      ILVO     | RNA2         | MH727573                          | 8.2                                              |                                               |
|      ILVO     | RNA3         | MH727574                          | 15.2                                             |                                               |
|      ILVO     | RNA4         | MH727575                          | 16.1                                             |                                               |
|      ILVO     | RNA5a        | MH727576                          | 8.3                                              |                                               |
|      ILVO     | RNA5b        | MH727577                          | 7.7                                              |                                               |
|      ILVO     | RNA6         | MH727578                          | 9.8                                              |                                               |
|      ILVO     | RNA7         | MH727579                          | 7.6                                              |                                               |
|      USDA-APHIS     | RNA1         | MH727572                          | 12.4                                               | yes                                           |
|      USDA-APHIS      | RNA2         | MH727573                          | 3.6                                              |                                               |
|      USDA-APHIS      | RNA3         | MH727574                          | 25.5                                             |                                               |
|      USDA-APHIS      | RNA4         | MH727575                          | 27.7                                             |                                               |
|      USDA-APHIS      | RNA5a        | MH727576                          | 4.2                                              |                                               |
|      USDA-APHIS      | RNA5b        | MH727577                          | 4.4                                              |                                               |
|      USDA-APHIS      | RNA6         | MH727578                          | 16.6                                              |                                               |
|      USDA-APHIS      | RNA7         | MH727579                          | 5.5                                              |                                               |
|      ValGenetics     | RNA1         | MH727572                          | 13.14                                              | yes                                           |
|      ValGenetics      | RNA2         | MH727573                          | 5.47                                              |                                               |
|      ValGenetics      | RNA3         | MH727574                          | 1.84                                            |                                               |
|      ValGenetics      | RNA4         | MH727575                          | 36.97                                             |                                               |
|      ValGenetics      | RNA5a        | MH727576                          | 5.58                                              |                                               |
|      ValGenetics      | RNA5b        | MH727577                          | 5.72                                              |                                               |
|      ValGenetics      | RNA6         | MH727578                          | 24.60                                             |                                               |
|      ValGenetics      | RNA7         | MH727579                          | 6.69                                             |                                               |

<sup>1</sup> This number represents the number of virus filtered reads mapped for each genomic segment against the total number of virus filtered reads.


### Comments of different labs while analyzing the dataset

Here you can read some comments the participants had while analyzing the dataset.

No comments yet.

### How to participate to the VIROMOCK challenge

The dataset can be downloaded [here](https://doi.org/10.5061/dryad.0zpc866z8) (click the arrow next to "November 2, 2021" to download each dataset separately).

If you finish your analysis, we encourage you to submit your results through [this Google Sheet](https://docs.google.com/spreadsheets/d/1Io3NfUrBRQTv1G0CL8MxDhslpJzNJrgldrXHngfq1MU/edit#gid=0).

The Google Sheet will allow you to share your results in detail. Only the green columns are required. However, we encourage you to give as much information as possible.

After submission of the Google Sheet, your results will be processed and added to Table 2.
